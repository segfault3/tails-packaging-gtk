-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gtk+3.0
Binary: libgtk-3-0, libgtk-3-0-udeb, libgtk-3-common, libgtk-3-bin, libgtk-3-dev, libgtk-3-doc, gtk-3-examples, gir1.2-gtk-3.0, gtk-update-icon-cache, libgail-3-0, libgail-3-dev, libgail-3-doc
Architecture: any all
Version: 3.22.11-1.0tails1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>
Homepage: http://www.gtk.org/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gtk+3.0/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gtk+3.0/
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, python3-gi, xauth, xvfb
Build-Depends: debhelper (>= 9.20141010), cdbs (>= 0.4.93), gnome-pkg-tools (>= 0.11), dpkg-dev (>= 1.17.14), gtk-doc-tools (>= 1.20), dh-autoreconf, docbook-xml, docbook-xsl, pkg-config, autotools-dev, dbus <!nocheck>, gsettings-desktop-schemas <!nocheck>, adwaita-icon-theme <!nocheck>, at-spi2-core <!nocheck>, libglib2.0-dev (>= 2.49.4), libgdk-pixbuf2.0-dev (>= 2.30.0), libpango1.0-dev (>= 1.37.3), libatk1.0-dev (>= 2.15.1), libatk-bridge2.0-dev, libegl1-mesa-dev [linux-any], libepoxy-dev, libfontconfig1-dev, libharfbuzz-dev (>= 0.9), libwayland-dev (>= 1.9.91) [linux-any], wayland-protocols (>= 1.7) [linux-any], libxkbcommon-dev (>= 0.2.0), libx11-dev, libxext-dev, libxi-dev, libxml2-utils, libxrandr-dev (>= 2:1.5.0), libxcursor-dev, libxcomposite-dev, libxdamage-dev, libxkbfile-dev, libxinerama-dev, libxfixes-dev, libcairo2-dev (>= 1.14.0), libcups2-dev (>= 1.2), libcolord-dev (>= 0.1.9), librest-dev, libjson-glib-dev, gobject-introspection (>= 1.41.3), libgirepository1.0-dev (>= 1.39.0), xauth <!nocheck>, xsltproc, xvfb <!nocheck>
Build-Depends-Indep: libglib2.0-doc, libatk1.0-doc, libpango1.0-doc, libcairo2-doc
Package-List:
 gir1.2-gtk-3.0 deb introspection optional arch=any
 gtk-3-examples deb x11 extra arch=any
 gtk-update-icon-cache deb misc optional arch=any
 libgail-3-0 deb libs optional arch=any
 libgail-3-dev deb libdevel optional arch=any
 libgail-3-doc deb doc optional arch=all
 libgtk-3-0 deb libs optional arch=any
 libgtk-3-0-udeb udeb debian-installer extra arch=any
 libgtk-3-bin deb misc optional arch=any
 libgtk-3-common deb misc optional arch=all
 libgtk-3-dev deb libdevel optional arch=any
 libgtk-3-doc deb doc optional arch=all
Checksums-Sha1:
 34204e745c137a31b93a74c72b7f6232d9b299ce 18250068 gtk+3.0_3.22.11.orig.tar.xz
 fbeb50a4bf0a48729ce47c66e827ac7f2b460501 144132 gtk+3.0_3.22.11-1.0tails1.debian.tar.xz
Checksums-Sha256:
 db440670cb6f3c098b076df3735fbc4e69359bd605385e87c90ee48344a804ca 18250068 gtk+3.0_3.22.11.orig.tar.xz
 586fc64d73fb12bd139fbaf5afeea4126bceb993a0bdf223a29bdbdd4b903b1f 144132 gtk+3.0_3.22.11-1.0tails1.debian.tar.xz
Files:
 8fe8031c12c4f14d8ae7cd4256b0e3ce 18250068 gtk+3.0_3.22.11.orig.tar.xz
 7e3b70875503410c1b9e4f214f25971e 144132 gtk+3.0_3.22.11-1.0tails1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEUrafEKOweFrQWvtHHYTM8BDMW8cFAlm5ovwSHGFub255bUBy
aXNldXAubmV0AAoJEB2EzPAQzFvHAzkP/2xaNXRWGgIi6cp4bm7UdkqA9l5NSII6
nTW5bf9is84NAQHoSxOcNMTtA+rSnHJuRfy8CvwBkm2mC9CD1MCrWNmdPg0n1vvs
JDOAVlreJ9SdkOoSJR+0VNYohBls86Vpe2/fy1RX4Ft8c9/7xifuj/6ux5reXkbJ
g4t/YgVRWBISfQckQL7Yoj/irI9JEZ6ADrIu7ojoIl3o9hyvEHHgHO5VhAoAmkvF
Uxc+4d8qYxYzRkeA/BLXE6f2OcjNSqDPcn/jLMv3vl+WwaAadqvvxbVpXPGYygDG
J5f7WxsFsA1GyKipaPc/YMWL5XL2uUNQGQgyYH685iqSYkO917B0bHepFMXiTSrM
IAapvU8y3MG95t08zXVBTbpXZKcX2a/FY59SabXFtr2hDHWwHfp16/PQ+QVPJmNt
JhQfaIqjGhyEOn+WIqg8OLUMKcSaWtuK8MSjaps6FcMQCCzHG783TPornF1pVoj2
oGEh6IDcUPGz8D6aM7TN+7QRjKqvbEsK6Vdn875551kj8zqRpZXthpHlYwxVQk1m
aWUFY1pEjaGV9vobIlf0OdUqI6jEsTe6fQgh3Hj46j97jOBhS+z4EvXo66w9LbEy
x32f4pAognu+obgM6/jda+juDs3Ysnj5Wn81yuhFMiAKqVykn5tmIQXWFY8Y6k8H
BxHBnaoTWwBU
=jsjv
-----END PGP SIGNATURE-----
